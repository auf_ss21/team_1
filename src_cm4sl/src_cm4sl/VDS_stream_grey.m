%   TCP/IP Verbindung zu IPG Carmaker über VDS
%   Genauere Beschreibung der Datenstruktur in IPG Movie Doku 
%clear ;
%close all;


%localhost wen IPG auf gleichem Rechner läuft
Adress = "localhost";
%Port wird in VDS.cfg gesetzt (default 2210)
port = 2210;            
client_connected = false;
got_image = false;
%TCP Verbindung erstellen
vds_client = tcpclient(Adress, port);  

% Ersten String von Carmaker Empfangen
% bei erfolgreicher Verbindung -> "*IPGMovie 9.1.1 12/16/20\n"
data = read(vds_client,64,"char");
if contains(data, "*IPGMovie")
   disp("IPG Movie is Connected...");
   client_connected = true;
   disp(data);
end

%Empfangen der Bildinformationen + Bild
%Erwartete Struktur -> "*VDS <no> <fmt> <time> <wi>x<he> <len>\n" + Data 
while(got_image == false)
    while(1)
      if client_connected && contains(data, "*VDS")
        got_image = true;
        % Daten String emfangen (Länge = Info(max 64) + Daten(w*h*Layer)
        img_data = read(vds_client,360000,"uint8");
        meta_data = read(vds_client,64,"char");
        
        %disp(meta_data);
        %disp(img_data);
        %Daten aufteilen über Leerzeichen
        splitdata = strsplit(meta_data," ");
        
        %imgtype = splitdata(3);
        timestamp = splitdata(4);
        %disp(timestamp);
        %img_size = splitdata(5);
        %data_len = splitdata(6);
        %img_size_split = split(img_size,"x");
        %image_w = img_size_split(1);
        %imag_h = img_size_split(2);

        %Extrahierten der Bilddaten
        %img = extractBefore(splitdata{1,1}, "*VDS");
        %Reshape in Matrix
        img_matrix = reshape(img_data,[600,600])';
        % Konvertierten zu Double
        %pic = double(img_matrix);
    
        laneBoundaryCell = detectLanes(img_matrix,600,600,timestamp);
        %laneBoundary = [1,2];
        timeCell = laneBoundaryCell(1);
        %laneBoundary.time = timeCell{1};
        dataCell = laneBoundaryCell(2);
        %laneBoundary.signals.values = dataCell{1};
        %flush(laneBoundary);
        laneBoundary = 5.2;
        disp(laneBoundary);

    else 
        pause(0.1);
        data = read(vds_client,64,"char");
        disp(data);
      end
    end
end


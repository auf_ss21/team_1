#include "Lineds.h"
#include <CarMaker.h>
#include "Vehicle/Sensor_Line.h"

/*  
** call in User_DeclQuants ()
*/   

void
Lineds_DeclQuants (void)
{

    DDefDouble4  (NULL, "LineL1_p1_x",	"m", &Lds.L1.p1.x, DVA_None);
    DDefDouble4  (NULL, "LineL1_p1_y",	"m", &Lds.L1.p1.y, DVA_None);
    DDefDouble4  (NULL, "LineL1_p1_z",	"m", &Lds.L1.p1.z, DVA_None);
    DDefDouble4  (NULL, "LineL1_p2_x",	"m", &Lds.L1.p2.x, DVA_None);
    DDefDouble4  (NULL, "LineL1_p2_y",	"m", &Lds.L1.p2.y, DVA_None);
    DDefDouble4  (NULL, "LineL1_p2_z",	"m", &Lds.L1.p2.z, DVA_None);
    
    DDefDouble4  (NULL, "LineL1_p3_x",	"m", &Lds.L1.p3.x, DVA_None);
    DDefDouble4  (NULL, "LineL1_p3_y",	"m", &Lds.L1.p3.y, DVA_None);
    DDefDouble4  (NULL, "LineL1_p3_z",	"m", &Lds.L1.p3.z, DVA_None);    
    
    
    
    DDefDouble4  (NULL, "LineL2_p1_x",	"m", &Lds.L2.p1.x, DVA_None);
    DDefDouble4  (NULL, "LineL2_p1_y",	"m", &Lds.L2.p1.y, DVA_None);
    DDefDouble4  (NULL, "LineL2_p1_z",	"m", &Lds.L2.p1.z, DVA_None);
    DDefDouble4  (NULL, "LineL2_p2_x",	"m", &Lds.L2.p2.x, DVA_None);
    DDefDouble4  (NULL, "LineL2_p2_y",	"m", &Lds.L2.p2.y, DVA_None);
    DDefDouble4  (NULL, "LineL2_p2_z",	"m", &Lds.L2.p2.z, DVA_None);
    DDefDouble4  (NULL, "LineR1_p1_x",	"m", &Lds.R1.p1.x, DVA_None);
    DDefDouble4  (NULL, "LineR1_p1_y",	"m", &Lds.R1.p1.y, DVA_None);
    DDefDouble4  (NULL, "LineR1_p1_z",	"m", &Lds.R1.p1.z, DVA_None);
    DDefDouble4  (NULL, "LineR1_p2_x",	"m", &Lds.R1.p2.x, DVA_None);
    DDefDouble4  (NULL, "LineR1_p2_y",	"m", &Lds.R1.p2.y, DVA_None);
    DDefDouble4  (NULL, "LineR1_p2_z",	"m", &Lds.R1.p2.z, DVA_None);
    DDefDouble4  (NULL, "LineR2_p1_x",	"m", &Lds.R2.p1.x, DVA_None);
    DDefDouble4  (NULL, "LineR2_p1_y",	"m", &Lds.R2.p1.y, DVA_None);
    DDefDouble4  (NULL, "LineR2_p1_z",	"m", &Lds.R2.p1.z, DVA_None);
    DDefDouble4  (NULL, "LineR2_p2_x",	"m", &Lds.R2.p2.x, DVA_None);
    DDefDouble4  (NULL, "LineR2_p2_y",	"m", &Lds.R2.p2.y, DVA_None);
    DDefDouble4  (NULL, "LineR2_p2_z",	"m", &Lds.R2.p2.z, DVA_None);
}


/*
** Lineds_TestRun_Start_atEnd ()
**
** initialises struct at the start of a new TestRun
**
** call in User_TestRun_Start_atEnd ()
*/
int
Lineds_TestRun_Start_atEnd (void)
{
     Lds.L1.p1.x = 0; 
     Lds.L1.p1.y = 0; 
     Lds.L1.p1.z = 0;
     Lds.L1.p2.x = 0;
     Lds.L1.p2.y = 0;
     Lds.L1.p2.z = 0;
     
     Lds.L1.p3.x  = 0;
     Lds.L1.p3.y  = 0;
     Lds.L1.p3.z  = 0;        
     
     Lds.L2.p1.x = 0;
     Lds.L2.p1.y = 0;
     Lds.L2.p1.z = 0;
     Lds.L2.p2.x = 0; 
     Lds.L2.p2.y = 0; 
     Lds.L2.p2.z = 0;
     Lds.R1.p1.x = 0;
     Lds.R1.p1.y = 0;
     Lds.R1.p1.z = 0;
     Lds.R1.p2.x = 0;
     Lds.R1.p2.y = 0;
     Lds.R1.p2.z = 0;
     Lds.R2.p1.x = 0; 
     Lds.R2.p1.y = 0; 
     Lds.R2.p1.z = 0;
     Lds.R2.p2.x = 0;
     Lds.R2.p2.y = 0;
     Lds.R2.p2.z = 0;
     
    return 0;
}    
     
     
/*  
** call in User_Calc ()
*/   
     
int  
Lineds_Calc (double dt)
{    
     
    /*Execute only during simulation*/
    if (SimCore.State != SCState_Simulate) return 0;
     
    /*Execute only if at least 1 line is detected on the left or  right side*/
    if (LineSensor[0].RLines.nLine == 0 || LineSensor[0].LLines.nLine == 0) {
       Lds.L1.p1.x  = 0; 
       Lds.L1.p1.y  = 0; 
       Lds.L1.p1.z  = 0;
       Lds.L1.p2.x  = 0;
       Lds.L1.p2.y  = 0;
       Lds.L1.p2.z  = 0;
       
       Lds.L1.p3.x  = 0;
       Lds.L1.p3.y  = 0;
       Lds.L1.p3.z  = 0;       
       
       Lds.L2.p1.x  = 0;
       Lds.L2.p1.y  = 0;
       Lds.L2.p1.z  = 0;
       Lds.L2.p2.x  = 0; 
       Lds.L2.p2.y  = 0; 
       Lds.L2.p2.z  = 0;
       Lds.R1.p1.x  = 0;
       Lds.R1.p1.y  = 0;
       Lds.R1.p1.z  = 0;
       Lds.R1.p2.x  = 0;
       Lds.R1.p2.y  = 0;
       Lds.R1.p2.z  = 0;
       Lds.R2.p1.x  = 0; 
       Lds.R2.p1.y  = 0; 
       Lds.R2.p1.z  = 0;
       Lds.R2.p2.x  = 0;
       Lds.R2.p2.y  = 0;
       Lds.R2.p2.z  = 0;

        return 0;
    };

        Lds.L1.p1.x = LineSensor[0].LLines.L[0].ds[0][0];
        Lds.L1.p1.y = LineSensor[0].LLines.L[0].ds[0][1];
        Lds.L1.p1.z = LineSensor[0].LLines.L[0].ds[0][2];
        Lds.L1.p2.x = LineSensor[0].LLines.L[0].ds[1][0];
        Lds.L1.p2.y = LineSensor[0].LLines.L[0].ds[1][1];
        Lds.L1.p2.z = LineSensor[0].LLines.L[0].ds[1][2];
        
        Lds.L1.p3.x = LineSensor[0].LLines.L[0].ds[2][0];
        Lds.L1.p3.y = LineSensor[0].LLines.L[0].ds[2][1];
        Lds.L1.p3.z = LineSensor[0].LLines.L[0].ds[2][2];
        
        Lds.L2.p1.x = LineSensor[0].LLines.L[1].ds[0][0];
        Lds.L2.p1.y = LineSensor[0].LLines.L[1].ds[0][1];
        Lds.L2.p1.z = LineSensor[0].LLines.L[1].ds[0][2];
        Lds.L2.p2.x = LineSensor[0].LLines.L[1].ds[1][0];
        Lds.L2.p2.y = LineSensor[0].LLines.L[1].ds[1][1];
        Lds.L2.p2.z = LineSensor[0].LLines.L[1].ds[1][2];
        Lds.R1.p1.x = LineSensor[0].RLines.L[0].ds[0][0];
        Lds.R1.p1.y = LineSensor[0].RLines.L[0].ds[0][1]; 
        Lds.R1.p1.z = LineSensor[0].RLines.L[0].ds[0][2];  
        Lds.R1.p2.x = LineSensor[0].RLines.L[0].ds[1][0];  
        Lds.R1.p2.y = LineSensor[0].RLines.L[0].ds[1][1];
        Lds.R1.p2.z = LineSensor[0].RLines.L[0].ds[1][2];
        Lds.R2.p1.x = LineSensor[0].RLines.L[1].ds[0][0]; 
        Lds.R2.p1.y = LineSensor[0].RLines.L[1].ds[0][1]; 
        Lds.R2.p1.z = LineSensor[0].RLines.L[1].ds[0][2]; 
        Lds.R2.p2.x = LineSensor[0].RLines.L[1].ds[1][0]; 
        Lds.R2.p2.y = LineSensor[0].RLines.L[1].ds[1][1];  
        Lds.R2.p2.z = LineSensor[0].RLines.L[1].ds[1][2];  

        return 0;
}
